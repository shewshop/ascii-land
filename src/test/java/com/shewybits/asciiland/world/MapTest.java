package com.shewybits.asciiland.world;

import java.io.File;

import junit.framework.TestCase;

public class MapTest extends TestCase {
	
	private Map testMap;
	
	public MapTest(String name) {
		super(name);
		testMap = new Map("testing");
		testMap.load();
	}
	
	public void testLoad() throws Exception {
		assertEquals("testing", testMap.getMapName());
		assertEquals(0, testMap.getActiveRoom());
		assertEquals(3, testMap.getNumRooms());
		assertEquals("welcome_room", testMap.getRooms().get(-3).getRoomName());
	}
	
	public void testSave() throws Exception {
		testMap.setMapName("testing-save");
		testMap.save();
		
		String fileSep = System.getProperty("file.separator");
		File toLoad = new File("." + fileSep + "src" + fileSep + "test" + fileSep + "resources" + fileSep + "maps" + fileSep + "testing-save" + fileSep + "map_info.txt");
		
		assertTrue(toLoad.exists());
		
		testMap.load();
		
		assertEquals("testing-save", testMap.getMapName());
		assertEquals(0, testMap.getActiveRoom());
		assertEquals(3, testMap.getNumRooms());
		assertEquals("welcome_room", testMap.getRooms().get(-3).getRoomName());
		
		testMap.setMapName("testing");
	}
	
}
