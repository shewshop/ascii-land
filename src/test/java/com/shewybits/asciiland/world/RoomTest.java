package com.shewybits.asciiland.world;

import java.util.Arrays;

import junit.framework.TestCase;

public class RoomTest extends TestCase {
	
	int roomTestId1 = -1, testRoomId2 = -3;
	Room testRoom;
	
	public RoomTest(String name) {
		super(name);
		testRoom = new Room(roomTestId1);
		testRoom.load("testing");
	}
	
	public void testLoad() throws Exception {		
		assertEquals("testing", testRoom.getMapName());
		assertEquals(roomTestId1, testRoom.getRoomId());
		assertEquals(100, testRoom.getRoomWidth());
		assertEquals(23, testRoom.getRoomHeight());
		assertEquals("welcome_room", testRoom.getRoomName());
		assertEquals('X', testRoom.getDefaultChar());
		assertEquals('D', testRoom.getTileAt(5, 3));
		assertEquals(1, testRoom.getTileCodeAt(3, 20));
		assertEquals(2, testRoom.getConnections().size());
		assertTrue(testRoom.getConnection(0).equals(new Connection("testing", -1, 60, 9, "testing", -2, 5, 13)));
	}
	
	public void testSave() throws Exception {
		testRoom.setRoomId(testRoomId2);
		testRoom.save("testing");
		testRoom.load("testing");
		testRoom.setRoomId(roomTestId1);
		
		String expectedTiles = "                                  - WELCOME TO ASCIILAND -                                          \nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww--------------wwwwwwwwwwwwwwwwwwwwwwwX\nXww  Diag. 3:              wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/                \\wwwwwwwwwwwwwwwwwwwwwX\nXww An ASCIILander Himself wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/   Diag. 1:       \\wwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/ An ASCIILander's   \\wwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/     House            \\wwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\\                      /wwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww__\\                    /wwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww  >                 /wwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww__>               /wwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\\______________/wwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwww.............................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXww..........................................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX.......~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX...~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX..~~~~~~~~~~~     Diag. 2:      ~~~~~~~~~~~~............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX~~~~~~~~~~~~~ An ASCIILAND Pond ~~~~~~~~~~~~~~~~~~...............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
		assertEquals(expectedTiles, testRoom.getTileTextRaw());
	}
	
	public void testConnections() throws Exception {
		Room expected = new Room(-2, "testing");
		expected.load();
		Room house = new Room(testRoom.getConnection(1).getToRoomId(), testRoom.getConnection(1).getToMap());
		house.load();
		
		assertEquals(expected.getMapName(), house.getMapName());
		assertEquals(expected.getRoomId(), house.getRoomId());
		assertEquals(expected.getRoomName(), house.getRoomName());
		assertEquals(expected.getTileTextRaw(), house.getTileTextRaw());
		assertTrue(expected.getConnections().equals(house.getConnections()));
		assertTrue(expected.equals(house));
	}
	
	public void testTileTextRaw() throws Exception {
		String expected = "                                  - WELCOME TO ASCIILAND -                                          \nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww--------------wwwwwwwwwwwwwwwwwwwwwwwX\nXww  Diag. 3:              wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/                \\wwwwwwwwwwwwwwwwwwwwwX\nXww An ASCIILander Himself wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/   Diag. 1:       \\wwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/ An ASCIILander's   \\wwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/     House            \\wwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\\                      /wwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww__\\                    /wwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww  >                 /wwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww__>               /wwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\\______________/wwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXwwwwww.............................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXww..........................................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX.......~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX...~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX..~~~~~~~~~~~     Diag. 2:      ~~~~~~~~~~~~............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nX~~~~~~~~~~~~~ An ASCIILAND Pond ~~~~~~~~~~~~~~~~~~...............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
		
		assertEquals(expected, testRoom.getTileTextRaw());
	}
	
	public void testTileText() throws Exception {
		String[] expected = new String[]{"                                  - WELCOME TO ASCIILAND -                                          \n", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww--------------wwwwwwwwwwwwwwwwwwwwwwwX\n", "Xww  Diag. 3:              wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/                \\wwwwwwwwwwwwwwwwwwwwwX\n", "Xww An ASCIILander Himself wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/   Diag. 1:       \\wwwwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/ An ASCIILander's   \\wwwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww/     House            \\wwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\\                      /wwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww__\\                    /wwwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww  >                 /wwwwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww__>               /wwwwwwwwwwwwwwwwwwwwwX\n", "Xwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\\______________/wwwwwwwwwwwwwwwwwwwwwwX\n", "XwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "XwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "XwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "XwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "Xwwwwww.............................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "Xww..........................................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "X.......~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "X...~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "X..~~~~~~~~~~~     Diag. 2:      ~~~~~~~~~~~~............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "X~~~~~~~~~~~~~ An ASCIILAND Pond ~~~~~~~~~~~~~~~~~~...............wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwX\n", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n"};
				
		assertTrue(Arrays.equals(expected, testRoom.getTileText()));
	}
	
}
