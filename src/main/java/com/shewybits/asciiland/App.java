package com.shewybits.asciiland;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;
import com.shewybits.asciiland.world.Connection;
import com.shewybits.asciiland.world.Map;

/**
 * Hello world!
 *
 */
public class App {
	
	static final int WIDTH = 128, HEIGHT = 32;
	
	static SwingTerminal terminal;
	static Screen screen;
	
	static Map map;
	
	public App() {
		System.out.printf("Initializing screen...\n");
    	
    	initScreen();
    	
    	System.out.printf("Initializing map...\n");
    	
    	int x = 10, y = 10;
    	try {
			initMap();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	while (true) {
    		Key key = screen.readInput();
    		
    		if (key != null) {
    			switch (key.getKind()) {
        		case Escape:
        			screen.stopScreen();
        			System.exit(0);
        			break;
        		case ArrowLeft:
        			x -= 1;
        			if (!canWalkOn(x, y)) {
        				x += 1;
        			}
        			break;
        		case ArrowRight:
        			x += 1;
        			if (!canWalkOn(x, y)) {
        				x -= 1;
        			}
        			break;
        		case ArrowUp:
        			y--;
        			if (!canWalkOn(x, y)) {
        				y++;
        			}
        			break;
        		case ArrowDown:
        			y++;
        			if (!canWalkOn(x, y)) {
        				y--;
        			}
        			break;
    			default:
    				break;
        		}
    		}
    		
    		Connection toWarp = warpTo(x, y);
    		
    		if (toWarp != null) {
    			System.out.printf("Warping player to room %d @ %d,%d...\n", toWarp.getToRoomId(), toWarp.getToX(), toWarp.getToY());
    			
    			map.setActiveRoom(toWarp.getToRoomId());
    			x = toWarp.getToX();
    			y = toWarp.getToY();    			
    		}
    		
    		screen.clear();
    		map.drawRelative(screen, x, y);
    		drawSquare(screen.getTerminalSize().getColumns() / 2, screen.getTerminalSize().getRows() / 2);
    		screen.refresh();
    	}

	}
	
    public static void main( String[] args ) {
    	new App();
    }
    
    public Connection warpTo(int x, int y) {
    	ArrayList<Connection> connections = map.getRooms().get(map.getActiveRoom()).getConnections();
    	
    	for (Connection c : connections) {
    		if (c.getFromX() == x && c.getFromY() == y) {
    			return c;
    		}
    	}
    	
    	return null;
    }
    
    public boolean canWalkOn(int x, int y) {
    	return map.getRooms().get(map.getActiveRoom()).getTileCodeAt(x, y) == 0;
    }
    
    public void initMap() throws IOException {
    	map = new Map("demo");
    	map.load();
    	map.setActiveRoom(1);
    }
    
    public void initScreen() {
    	terminal = TerminalFacade.createSwingTerminal(WIDTH, HEIGHT);
    	terminal.setCursorVisible(false);
    	screen = TerminalFacade.createScreen(terminal);
    	screen.getTerminal().setCursorVisible(false);
    	screen.setCursorPosition(null);
    	screen.startScreen();
    	screen.updateScreenSize();
    	screen.refresh();
    	InputStream is = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream("images/icon.gif"));
    	try {
			terminal.getJFrame().setIconImage(ImageIO.read(is));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	terminal.getJFrame().setTitle("ASCII Land: A Romance of Many Bits");
    }
    
    public  void drawSquare(int x, int y) {
    	screen.putString(x, y, "[]", Terminal.Color.BLACK, Terminal.Color.WHITE);

    }
}
