package com.shewybits.asciiland.world;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.googlecode.lanterna.screen.Screen;

public class Map extends AbstractMap {
	
	public Map(String mapName) {
		this.mapName = mapName;
	}
	
	@Override
	public void drawRelative(Screen screen, int x, int y) {
		if (rooms == null || !rooms.containsKey(activeRoom)) {
			return;
		}
		
		rooms.get(activeRoom).drawRelative(screen, x, y);
	}
	
	@Override
	public void drawFrom(Screen screen, int x, int y) {
		if (rooms == null || !rooms.containsKey(activeRoom)) {
			return;
		}
		
		rooms.get(activeRoom).drawFrom(screen, x, y);
	}
	
	@Override
	public void drawRaw(Screen screen) {
		if (rooms == null || !rooms.containsKey(activeRoom)) {
			return;
		}
		
		rooms.get(activeRoom).drawRaw(screen);
	}

	@Override
	public void load() {
		try {
			System.out.printf("\tLoading map %s...\n", mapName);
			
			String fileSep = System.getProperty("file.separator");
			File fileToLoad;
			InputStream toLoad;
			BufferedReader in;
			
			if (mapName.contains("testing")) {
				fileToLoad = new File("." + fileSep + "src" + fileSep + (mapName.contains("testing") ? "test" : "main") + fileSep + "resources" + fileSep + "maps" + fileSep + mapName + fileSep + "map_info.txt");
				in = new BufferedReader(new FileReader(fileToLoad));
			} else {
				toLoad = getClass().getClassLoader().getResourceAsStream("maps/" + mapName + "/map_info.txt");
				in = new BufferedReader(new InputStreamReader(toLoad));
			}
			
			StringTokenizer st = new StringTokenizer(in.readLine());
			
			System.out.printf("\t\tFound map file...\n");
			
			setMapName(st.nextToken());
			st = new StringTokenizer(in.readLine());
			numRooms = Integer.parseInt(st.nextToken());
			rooms = new HashMap<Integer, Room>(numRooms);
			
			System.out.printf("\tLoading %d rooms for map '%s'\n", numRooms, mapName);
			
			for (int i = 0; i < numRooms; i++) {
				st = new StringTokenizer(in.readLine());
				int roomId = Integer.parseInt(st.nextToken());
				String roomName = st.nextToken();
				
				Room toAdd = new Room(roomId, roomName);
				toAdd.setMapName(mapName);
				toAdd.load();
				
				rooms.put(roomId, toAdd);
			}
			
			in.close();
			
			System.out.printf("\tDone loading map %s!\n", mapName);
		} catch (IOException ioe) {
			System.err.printf("\tFailed to load map '%s'.\n\tTrace:\n", mapName);
			ioe.printStackTrace();
		} catch (Exception e) {
			System.err.println("\tFound map to load, but it was not correctly formatted.\n\tTrace:\n");
			e.printStackTrace();
		}
	}

	@Override
	public void save() {
		try {
			String fileSep = System.getProperty("file.separator");
			(new File("." + fileSep + "src" + fileSep + (mapName.contains("test") ? "test" : "main") + fileSep + "resources" + fileSep + "maps" + fileSep + mapName + fileSep + "rooms")).mkdirs();
			File toSave = new File("." + fileSep + "src" + fileSep + (mapName.contains("test") ? "test" : "main") + fileSep + "resources" + fileSep + "maps" + fileSep + mapName + fileSep + "map_info.txt");
			toSave.createNewFile();
			BufferedWriter out = new BufferedWriter(new FileWriter(toSave));
			
			out.write(mapName + "\n");
			out.write(numRooms + "\n");
			
			for (Room roomToSave : rooms.values()) {
				out.write(roomToSave.getRoomId() + " " + roomToSave.getRoomName() + "\n");
				roomToSave.save(mapName);
			}
			
			out.close();
		} catch (IOException ioe) {
			System.out.printf("Failed to save map '%s'.\n", mapName);
		} catch (Exception e) {
			System.out.println("Found map to save, but it was not correctly formatted.");
		}
	}

	@Override
	public void loadRoom(int roomId) {
		if (rooms == null || rooms.isEmpty() || !rooms.containsKey(roomId)) {
			return;
		}
		
		rooms.get(roomId).load(mapName);
	}

	@Override
	public void saveRoom(int roomId) {
		if (rooms == null || rooms.isEmpty() || !rooms.containsKey(roomId)) {
			return;
		}
		
		rooms.get(roomId).save();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		
		Map other = (Map) o;
		
		return this.mapName.equals(other.getMapName()) && this.rooms.equals(other.getRooms());
	}
	
}
