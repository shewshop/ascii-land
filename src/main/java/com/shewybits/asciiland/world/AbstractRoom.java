package com.shewybits.asciiland.world;

import com.googlecode.lanterna.screen.Screen;

public abstract class AbstractRoom {
	
	protected String roomName, mapName;
	protected int roomId;
	protected int roomWidth, roomHeight;
	protected char defaultChar;
	protected Tile[][] tiles;
	protected int numConnections;
	
	public abstract void load();
	
	public abstract void load(String mapName);
	
	public abstract void save();
	
	public abstract void save(String mapName);
	
	public abstract void drawRelative(Screen screen, int x, int y);
	
	public abstract void drawFrom(Screen screen, int x, int y);
	
	public abstract void drawRaw(Screen screen);
	
	public abstract Tile getTileAt(int x, int y);
	
	public abstract int getTileCodeAt(int x, int y);
	
	public abstract void addConnection(Connection connection);
	
	public abstract void addConnection(String toMap, int toRoomId, int fromX, int fromY, int toX, int toY);
	
	public abstract void removeConnection(Connection connection);
	
	public abstract Connection getConnection(int idx);
	
	public abstract boolean hasConnection(Connection connection);
	
	public abstract String[] getTileText();
	
	public abstract String getTileTextRaw();
	
	public abstract String[] getTileCodeText();
	
	public abstract String getTileCodeTextRaw();
	
	public boolean outOfBounds(int x, int y) {
		return (x < 0 || x >= roomWidth || y < 0 || y >= roomHeight);
	}
	
	public String getRoomName() {
		return roomName;
	}
	
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
	public String getMapName() {
		return mapName;
	}
	
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public int getRoomWidth() {
		return roomWidth;
	}

	public void setRoomWidth(int roomWidth) {
		this.roomWidth = roomWidth;
	}

	public int getRoomHeight() {
		return roomHeight;
	}

	public void setRoomHeight(int roomHeight) {
		this.roomHeight = roomHeight;
	}
	
	public char getDefaultChar() {
		return defaultChar;
	}
	
	public void setDefaultChar(char defaultChar) {
		this.defaultChar = defaultChar;
	}
	
	public Tile[][] getTiles() {
		return tiles;
	}

	public void setTiles(Tile[][] tiles) {
		this.tiles = tiles;
	}
	
	public int getNumConnections() {
		return numConnections;
	}
	
	public void setNumConnections(int numConnections) {
		this.numConnections = numConnections;
	}
	
}
