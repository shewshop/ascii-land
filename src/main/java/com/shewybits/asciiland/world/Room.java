package com.shewybits.asciiland.world;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;

public class Room extends AbstractRoom {

	public Room(int roomId) {
		this.roomId = roomId;
	}
	
	public Room(int roomId, String mapName) {
		this.roomId = roomId;
		this.mapName = mapName;
	}
	
	@Override
	public void load() {
		load(mapName);
	}
	
	@Override
	public void load(String mapName) {
		try {
			System.out.printf("\t\tLoading room %d...\n", roomId);
			
			String fileSep = System.getProperty("file.separator");
			File fileToLoad;
			InputStream roomToLoad;
			BufferedReader in;
			
			if (roomId < 0) {
				fileToLoad = new File("." + fileSep + "src" + fileSep + "test" + fileSep + "resources" + fileSep + "maps" + fileSep + mapName + fileSep + "rooms" + fileSep + "room" + -getRoomId() + ".txt");
				in = new BufferedReader(new FileReader(fileToLoad));
			} else {
				roomToLoad = getClass().getClassLoader().getResourceAsStream("maps/" + mapName + "/rooms/room" + roomId + ".txt");
				in = new BufferedReader(new InputStreamReader(roomToLoad));
			}
			
			StringTokenizer st = new StringTokenizer(in.readLine());
			
			setMapName(mapName);
			roomHeight = Integer.parseInt(st.nextToken());
			roomWidth = Integer.parseInt(st.nextToken());
			roomName = st.nextToken();
			defaultChar = st.nextToken().charAt(0);
			
			System.out.printf("\t\tLoading tiles...\n");
			
			tiles = new Tile[roomWidth][roomHeight];
			
			for (int y = 0; y < roomHeight; y++) {
				String line = in.readLine();
				for (int x = 0; x < roomWidth; x++) {
					tiles[x][y] = new Tile(line.charAt(x));
				}
			}
			
			for (int y = 0; y < roomHeight; y++) {
				st = new StringTokenizer(in.readLine());
				for (int x = 0; x < roomWidth; x++) {
					tiles[x][y].setCode(Integer.parseInt(st.nextToken()));
				}
			}
			
			numConnections = Integer.parseInt(in.readLine());
			
			System.out.printf("\t\tLoading %d connections...\n", numConnections);

			for (int i = 0; i < numConnections; i++) {
				String fromMap, toMap;
				int fromRoomId, fromX, fromY, toRoomId, toX, toY;
				
				st = new StringTokenizer(in.readLine());
				
				fromMap = st.nextToken();
				fromRoomId = Integer.parseInt(st.nextToken());
				fromX = Integer.parseInt(st.nextToken());
				fromY = Integer.parseInt(st.nextToken());
				
				toMap = st.nextToken();
				toRoomId = Integer.parseInt(st.nextToken());
				toX = Integer.parseInt(st.nextToken());
				toY = Integer.parseInt(st.nextToken());
				
				Connection toAdd = new Connection(fromMap, fromRoomId, fromX, fromY, toMap, toRoomId, toX, toY);
				
				tiles[fromX][fromY].setConnection(toAdd);
				
			}
			
			System.out.printf("\t\tDone loading room %s!\n", roomName);

			in.close();
		} catch (IOException ioe) {
			System.err.printf("\t\tFailed to load room from map '%s' with id %d.\n\t\tTrace:\n", mapName, roomId);
			ioe.printStackTrace();
		} catch (Exception e) {
			System.err.println("\t\tFound room to load, but it was not correctly formatted.\n\t\tTrace:\n");
			e.printStackTrace();
		}
	}
	
	@Override
	public void save() {
		save(mapName);
	}

	@Override
	public void save(String mapName) {
		try {
			String fileSep = System.getProperty("file.separator");
			(new File("." + fileSep + "src" + fileSep + ((roomId < 0) ? "test" : "main") + fileSep + "resources" + fileSep + "maps" + fileSep + mapName + fileSep + "rooms")).mkdirs();
			File roomToSave = new File("." + fileSep + "src" + fileSep + ((roomId < 0) ? "test" : "main") + fileSep + "resources" + fileSep + "maps" + fileSep + mapName + fileSep + "rooms" + fileSep + "room" + Math.abs(getRoomId()) + ".txt");
			BufferedWriter out = new BufferedWriter(new FileWriter(roomToSave));
			
			out.write(roomHeight + " " + roomWidth + " " + roomName + " " + getDefaultChar() + "\n");
			
			String[] toWrite = getTileText();
			for (String line: toWrite) {
				out.write(line);
			}			
			
			toWrite = getTileCodeText();
			for (String line: toWrite) {
				out.write(line);
			}
			
			out.write(numConnections + "\n");
			
			for (Connection c : connections) {
				out.write(c.getFromMap() + " " + c.getFromRoomId() + " " + c.getFromX() + " " + c.getFromY() + " " + c.getToMap() + " " + c.getToRoomId() + " " + c.getToX() + " " + c.getToY() + "\n");
			}
			
			out.close();
		} catch (IOException ioe) {
			System.err.printf("Failed to save room from map '%s' with id %d.\n", mapName, roomId);
		} catch (Exception e) {
			System.err.println("Found room to save, but it was not correctly formatted.");
		}	
	}
	
	@Override
	public void drawRelative(Screen screen, int x, int y) {
		if (screen == null || tiles == null || outOfBounds(x, y)) {
			return;
		}
		
		int screenWidth = screen.getTerminalSize().getColumns();
		int screenHeight = screen.getTerminalSize().getRows();
		
		int deltaX = screenWidth / 2;
		int deltaY = screenHeight / 2;
		
		for (int xp = x - deltaX, xs = 0; xp <= x + deltaX; xp++, xs++) {
			for (int yp = y - deltaY, ys = 0; yp <= y + deltaY; yp++, ys++) {
				screen.putString(xs, ys, "" + getTileAt(xp, yp), Terminal.Color.DEFAULT, Terminal.Color.DEFAULT);
			}
		}
	}
	
	@Override
	public void drawFrom(Screen screen, int x, int y) {
		if (screen == null || tiles == null || outOfBounds(x, y)) {
			return;
		}
		
		int screenWidth = screen.getTerminalSize().getColumns();
		int screenHeight = screen.getTerminalSize().getRows();
		
		int deltaX = screenWidth / 2;
		int deltaY = screenHeight / 2;
		
		System.out.printf("Terminal Size: %d x %d", screenWidth, screenHeight);
		System.out.printf("Drawing from %d, %d to %d, %d centered at %d, %d.\n", x - deltaX, y - deltaY, x + deltaX, y + deltaY, x, y);
		
		for (int xp = x - deltaX; xp <= x + deltaX; xp++) {
			for (int yp = y - deltaY; yp <= x + deltaY; yp++) {
				screen.putString(xp, yp, "" + getTileAt(xp, yp), Terminal.Color.DEFAULT, Terminal.Color.DEFAULT);
			}
		}
	}

	@Override
	public void drawRaw(Screen screen) {
		if (screen == null || tiles == null) {		
			return;
		}
		
		for (int x = 0; x < tiles.length; x++) {
			for (int y = 0; y < tiles[0].length; y++) {
				screen.putString(x, y, "" + getTileAt(x, y), Terminal.Color.DEFAULT, Terminal.Color.DEFAULT);
			}
		}
	}

	@Override
	public Tile getTileAt(int x, int y) {
		if (tiles == null || tiles.length == 0 || tiles[0].length == 0 || outOfBounds(x, y)) {
			return new Tile(defaultChar);
		}
		return tiles[x][y];
	}

	@Override
	public int getTileCodeAt(int x, int y) {
		if (tiles == null || tiles.length == 0 || tiles[0].length == 0 || outOfBounds(x, y)) {
			return -1;
		}		
		return tiles[x][y].getCode();
	}
	
	@Override
	public String[] getTileText() {
		if (tiles == null) {
			return new String[]{"empty"};
		}
		
		String[] lines = new String[roomHeight];
		
		for (int y = 0; y < roomHeight; y++) {
			StringBuilder tileText = new StringBuilder();
			for (int x = 0; x < roomWidth; x++) {
				tileText.append(getTileAt(x, y));
			}
			tileText.append("\n");
			lines[y] = tileText.toString();
		}
		
		return lines;
	}
	
	@Override
	public String getTileTextRaw() {
		if (tiles == null) {
			return "";
		}
		
		StringBuilder tileText = new StringBuilder();
		
		for (int y = 0; y < roomHeight; y++) {
			for (int x = 0; x < roomWidth; x++) {
				tileText.append(getTileAt(x, y).getFace());
			}
			tileText.append("\n");
		}
		
		return tileText.toString();
	}
	
	@Override
	public String[] getTileCodeText() {
		if (tiles == null) {
			return new String[]{"empty"};
		}
		
		String[] lines = new String[roomHeight];
		
		for (int y = 0; y < roomHeight; y++) {
			StringBuilder tileText = new StringBuilder();
			for (int x = 0; x < roomWidth; x++) {
				tileText.append(getTileCodeAt(x, y) + " ");
			}
			tileText.append("\n");
			lines[y] = tileText.toString();
		}
		
		return lines;
	}
	
	@Override
	public String getTileCodeTextRaw() {
		if (tiles == null) {
			return "";
		}
		
		StringBuilder tileText = new StringBuilder();
		
		for (int y = 0; y < roomHeight; y++) {
			for (int x = 0; x < roomWidth; x++) {
				tileText.append(getTileCodeAt(x, y) + " ");
			}
			tileText.append("\n");
		}
		
		return tileText.toString();
	}

	@Override
	public void addConnection(Connection connection) {
		if (tiles == null || tiles.length == 0 || tiles[0].length == 0 || connection == null || tiles[connection.getFromX()][connection.getFromY()] == null) {
			return;
		}
		
		tiles[connection.getFromX()][connection.getFromY()].setConnection(connection);
	}

	@Override
	public void addConnection(String toMap, int toRoomId, int fromX, int fromY, int toX, int toY) {
		if (tiles == null || tiles.length == 0 || tiles[0].length == 0 || tiles[fromX][fromY] == null) {
			return;
		}
		
		String fileSep = System.getProperty("file.separator");
		File test = new File(".src" + fileSep + (toRoomId < 0 ? "test" : "main") + fileSep + "resources" + fileSep + "maps" + fileSep + toMap + fileSep + "rooms" + fileSep + "room" + Math.abs(toRoomId) + ".txt");
		if (test.exists()) {
			Connection toAdd = new Connection(mapName, roomId, fromX, fromY, toMap, toRoomId, toX, toY);
			
			tiles[fromX][fromY].setConnection(toAdd);
		}
	}

	@Override
	public void removeConnection(Connection connection) {
		if (tiles == null || tiles.length == 0 || tiles[0].length == 0 || !tiles[connection.getFromX()][connection.getFromY()].equals(connection)) {
			return;
		}
		
		tiles[connection.getFromX()][connection.getFromY()].setConnection(null);
	}
	
	@Override
	public boolean hasConnection(Connection connection) {
		if (connection == null || connections == null || connections.isEmpty()) {
			return false;
		}
		
		return connections.contains(connection);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		
		Room other = (Room) o;
		
		return this.roomName.equals(other.getRoomName()) && this.mapName.equals(other.getMapName()) && this.roomId == other.getRoomId() && this.roomHeight == other.getRoomHeight() && this.roomWidth == other.getRoomWidth() && this.getTileTextRaw().equals(other.getTileTextRaw()) && this.getTileCodeTextRaw().equals(other.getTileCodeTextRaw());
	}
		
}
