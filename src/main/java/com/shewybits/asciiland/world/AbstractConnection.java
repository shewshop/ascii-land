package com.shewybits.asciiland.world;

public abstract class AbstractConnection {
	
	protected String fromMap, toMap;
	protected int fromRoomId, toRoomId;
	protected int fromX, fromY, toX, toY;
	
	public String getFromMap() {
		return fromMap;
	}
	public void setFromMap(String fromMap) {
		this.fromMap = fromMap;
	}
	public String getToMap() {
		return toMap;
	}
	public void setToMap(String toMap) {
		this.toMap = toMap;
	}
	public int getFromRoomId() {
		return fromRoomId;
	}
	public void setFromRoomId(int fromRoomId) {
		this.fromRoomId = fromRoomId;
	}
	public int getToRoomId() {
		return toRoomId;
	}
	public void setToRoomId(int toRoomId) {
		this.toRoomId = toRoomId;
	}
	public int getFromX() {
		return fromX;
	}
	public void setFromX(int fromX) {
		this.fromX = fromX;
	}
	public int getFromY() {
		return fromY;
	}
	public void setFromY(int fromY) {
		this.fromY = fromY;
	}
	public int getToX() {
		return toX;
	}
	public void setToX(int toX) {
		this.toX = toX;
	}
	public int getToY() {
		return toY;
	}
	public void setToY(int toY) {
		this.toY = toY;
	}
	
}
