package com.shewybits.asciiland.world;

import java.util.Map;

import com.googlecode.lanterna.screen.Screen;

public abstract class AbstractMap {
	
	protected String mapName;
	protected int activeRoom, numRooms;
	protected Map<Integer, Room> rooms;
	
	public abstract void drawRelative(Screen screen, int x, int y);
	
	public abstract void drawFrom(Screen screen, int x, int y);
	
	public abstract void drawRaw(Screen screen);
	
	public abstract void load();
	
	public abstract void save();
	
	public abstract void loadRoom(int roomId);
	
	public abstract void saveRoom(int roomId);

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public Map<Integer, Room> getRooms() {
		return rooms;
	}

	public void setRooms(Map<Integer, Room> rooms) {
		this.rooms = rooms;
	}

	public int getActiveRoom() {
		return activeRoom;
	}

	public void setActiveRoom(int activeRoom) {
		this.activeRoom = activeRoom;
	}
	
	public int getNumRooms() {
		return numRooms;
	}
	
	public void setNumRooms(int numRooms) {
		this.numRooms = numRooms;
	}
	
}
