package com.shewybits.asciiland.world;

public class Tile extends AbstractTile {
	
	public Tile(char face) {
		this.face = face;
		this.code = 0;
	}
	
	public Tile(char face, int code) {
		this.face = face;
		this.code = code;
	}
	
	public Tile(char face, int code, Connection connection) {
		this.face = face;
		this.code = code;
		this.connection = connection;
	}
	
	@Override
	public boolean isWalkable() {
		return 0 != (WALKABLE_MASK & code);
	}

	@Override
	public boolean hasConnection() {
		return connection == null;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		
		Tile other = (Tile) o;
		
		return this.face == other.getFace() && this.code == other.getCode() && this.connection.equals(other.getConnection());
	}
	
}
