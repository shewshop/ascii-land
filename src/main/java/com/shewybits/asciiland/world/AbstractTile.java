package com.shewybits.asciiland.world;

public abstract class AbstractTile {
	
	protected static final int WALKABLE_MASK = 1;
	
	protected char face;
	protected int code;
	protected Connection connection;	
	
	public abstract boolean isWalkable();
	public abstract boolean hasConnection();
	
	public char getFace() {
		return face;
	}
	public void setFace(char face) {
		this.face = face;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Connection getConnection() {
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
}
