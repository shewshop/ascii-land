package com.shewybits.asciiland.world;

import java.io.IOException;
import java.util.Scanner;

public class SpaceAdder {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		
		while (in.hasNextLine()) {
			char[] line = in.nextLine().toCharArray();
			
			for (char c : line) {
				System.out.print(c + " ");
			}
			
			System.out.println();
		}
		
		in.close();
	}
}
