package com.shewybits.asciiland.world;

public class Connection extends AbstractConnection {
	
	public Connection (String fromMap, int fromRoomId, int fromX, int fromY, String toMap, int toRoomId, int toX, int toY) {
		this.fromMap = fromMap;
		this.fromRoomId = fromRoomId;
		this.toMap = toMap;
		this.toRoomId = toRoomId;
		this.fromX = fromX;
		this.fromY = fromY;
		this.toX = toX;
		this.toY = toY;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		
		Connection other = (Connection) o;
		
		return this.fromMap.equals(other.getFromMap()) && this.fromRoomId == other.getFromRoomId() && this.fromX == other.getFromX() && this.fromY == other.getFromY() && this.toMap.equals(other.getToMap()) && this.toRoomId == other.getToRoomId() && this.toX == other.getToX() && this.toY == other.getToY();
	}
	
}
