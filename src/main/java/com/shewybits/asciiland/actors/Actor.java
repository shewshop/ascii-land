package com.shewybits.asciiland.actors;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;

public class Actor extends AbstractActor {

	@Override
	public void load() {
		
	}
	
	@Override
	public void load(String id) {
		
	}
	
	@Override
	public void save() {
		
	}
	
	@Override
	public void save(String id) {
		
	}
	
	@Override
	public void draw(Screen screen) {
		if (screen == null || size == NULL) {
			return;
		}
		
		char[][] toDraw = getFig();
		
		for (int xp = 0; xp < width; xp++) {
			for (int yp = 0; yp < height; yp++) {
				screen.putString(screenx + xp, screeny + yp, "" + toDraw[xp][yp], Terminal.Color.DEFAULT, Terminal.Color.DEFAULT);
			}
		}
	}

	@Override
	public void move(int delta) {
		xcoord += delta * xvel;
		ycoord += delta & yvel;
	}

	@Override
	public boolean collidesWith(int x, int y) {
		return x >= xcoord && x <= xcoord + width && y >= ycoord && y <= ycoord + height; 
	}

	@Override
	public boolean collidesWith(Actor other) {
		return (xcoord <= other.getXcoord() + other.getWidth()) && (xcoord + width >= other.getXcoord()) && (ycoord <= other.getYcoord() + other.getHeight()) && (ycoord + height >= other.getYcoord());
	}	
	
}
