package com.shewybits.asciiland.actors;

import com.googlecode.lanterna.screen.Screen;

public abstract class AbstractActor {
	
	public static final int NULL = -1, SMALL = -2, MEDIUM = -3, LARGE = -4;
	
	protected String name, id;
	protected int xcoord, ycoord, xvel, yvel, screenx, screeny, size, height, width;
	protected char[][] figSmall, figMed, figLarge;
	
	public abstract void load();
	public abstract void load(String id);
	public abstract void save();
	public abstract void save(String id);
	public abstract void draw(Screen screen);
	public abstract void move(int delta);
	public abstract boolean collidesWith(int x, int y);
	public abstract boolean collidesWith(Actor other);
	
	public void shiftCamera(int x, int y) {
		screenx += x;
		screeny += y;
	}
	
	public void centerCamera(int scrWidth, int scrHeight) {
		screenx = (scrWidth / 2) - (width / 2);
		screeny = (scrHeight / 2) - (height / 2);
	}
	
	public char[][] getFig() {
		switch (size) {
		case SMALL:
			return figSmall;
		case MEDIUM:
			return figMed;
		case LARGE:
			return figLarge;
		default:
			return null;
		}
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getXcoord() {
		return xcoord;
	}
	public void setXcoord(int xcoord) {
		this.xcoord = xcoord;
	}
	public int getYcoord() {
		return ycoord;
	}
	public void setYcoord(int ycoord) {
		this.ycoord = ycoord;
	}
	public int getXvel() {
		return xvel;
	}
	public void setXvel(int xvel) {
		this.xvel = xvel;
	}
	public int getYvel() {
		return yvel;
	}
	public void setYvel(int yvel) {
		this.yvel = yvel;
	}
	public int getScreenx() {
		return screenx;
	}
	public void setScreenx(int screenx) {
		this.screenx = screenx;
	}
	public int getScreeny() {
		return screeny;
	}
	public void setScreeny(int screeny) {
		this.screeny = screeny;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
		
		switch (size) {
		case SMALL:
			this.width = figSmall.length;
			this.height = figSmall[0].length;
			break;
		case MEDIUM:
			this.width = figMed.length;
			this.height = figMed[0].length;
			break;
		case LARGE:
			this.width = figLarge.length;
			this.height = figLarge[0].length;
			break;
		default:
			this.width = 0;
			this.height = 0;
		}
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public char[][] getFigSmall() {
		return figSmall;
	}
	public void setFigSmall(char[][] figSmall) {
		this.figSmall = figSmall;
	}
	public char[][] getFigMed() {
		return figMed;
	}
	public void setFigMed(char[][] figMed) {
		this.figMed = figMed;
	}
	public char[][] getFigLarge() {
		return figLarge;
	}
	public void setFigLarge(char[][] figLarge) {
		this.figLarge = figLarge;
	}
	
}
